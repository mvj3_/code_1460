import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class Activity01 extends Activity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.main);

        TextView tv = new TextView(this);
        
        String myString = null;
        StringBuffer sff = new StringBuffer();
        try
        {
            Document doc = Jsoup.connect("http://192.168.64.9:8099/AgentJava/tt.jsp").get();
            Elements links = doc.select("a[href]");
            for(Element link : links){
                sff.append(link.attr("abs:href")).append("  ").append(link.text()).append("\n");
            }
            myString = sff.toString();
        }
        catch (Exception e)
        {

            myString = e.getMessage();
            e.printStackTrace();
        }
        /* 将信息设置到TextView */
        tv.setText(myString);
        
        /* 将TextView显示到屏幕上 */
        this.setContentView(tv);
    }
}